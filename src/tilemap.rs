use crate::ascii::TileName;
use crate::ascii::{spawn_ascii_sprite, AsciiSheet};
use crate::constants::TILE_SIZE;
use bevy::prelude::*;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
pub struct TileMapPlugin;

#[derive(Component)]
pub struct TileCollider;

impl Plugin for TileMapPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(create_simple_map);
    }
}

fn create_simple_map(mut commands: Commands, ascii: Res<AsciiSheet>) {
    let file = File::open("assets/map.txt").expect("No map file found");
    let color = Color::rgb(0.9, 0.9, 0.9);

    let mut tiles = Vec::new();

    for (y, line) in BufReader::new(file).lines().enumerate() {
        if let Ok(line) = line {
            for (x, char) in line.chars().enumerate() {
                let translation = Vec3::new(x as f32 * TILE_SIZE, -(y as f32) * TILE_SIZE, 100.0);
                let tile = spawn_ascii_sprite(
                    &mut commands,
                    &ascii,
                    TileName::Index(char as usize),
                    color.clone(),
                    translation,
                );

                if char == '#' {
                    commands.entity(tile).insert(TileCollider);
                }

                tiles.push(tile);
            }
        }
    }

    commands
        .spawn()
        .insert(Name::new("Map"))
        .insert(Transform::default())
        .insert(GlobalTransform::default())
        .push_children(&tiles);
}
