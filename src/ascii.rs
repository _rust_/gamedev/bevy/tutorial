use crate::constants::TILE_SIZE;
use bevy::prelude::*;

pub enum TileName {
    Background,
    Player,
    Index(usize),
}

pub struct AsciiPlugin;
pub struct AsciiSheet(pub Handle<TextureAtlas>);

impl Plugin for AsciiPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system_to_stage(StartupStage::PreStartup, load_acsii);
    }
}

pub fn spawn_ascii_sprite(
    commands: &mut Commands,
    ascii: &AsciiSheet,
    tile_name: TileName,
    color: Color,
    translation: Vec3,
) -> Entity {
    let index = match tile_name {
        TileName::Background => 0,
        TileName::Player => 1,
        TileName::Index(index) => index,
    };
    let mut sprite = TextureAtlasSprite::new(index);

    sprite.color = color;
    sprite.custom_size = Some(Vec2::splat(TILE_SIZE));

    commands
        .spawn_bundle(SpriteSheetBundle {
            sprite,
            texture_atlas: ascii.0.clone(),
            transform: Transform {
                translation,
                ..Default::default()
            },
            ..Default::default()
        })
        .id()
}

fn load_acsii(
    mut commands: Commands,
    assets: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
    let path = "Ascii.png";
    let image = assets.load(path);
    let atlas =
        TextureAtlas::from_grid_with_padding(image, Vec2::splat(9.0), 16, 16, Vec2::splat(2.0));

    let atlas_handle = texture_atlases.add(atlas);

    commands.insert_resource(AsciiSheet(atlas_handle));
}
