use crate::ascii::{spawn_ascii_sprite, AsciiSheet, TileName};
use crate::constants::TILE_SIZE;
use crate::tilemap::TileCollider;
use bevy::prelude::*;
use bevy::sprite::collide_aabb::collide;
// use bevy_inspector_egui::Inspectable;

pub struct PlayerPlugin;

// #[derive(Component, Inspectable)]
#[derive(Component)]
pub struct Player {
    speed: f32,
}

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(spawn_player)
            .add_system(camera_follow)
            .add_system(player_movement);
    }
}

fn camera_follow(
    player_query: Query<&Transform, With<Player>>,
    camera_query: Query<&mut Transform, (Without<Player>, With<Camera>)>,
) {
    let player_transform = player_query.single();
    let mut camera_transform = camera_query.single_mut();

    camera_transform.translation.x = player_transform.translation.x;
    camera_transform.translation.y = player_transform.translation.y;
}

fn player_movement(
    mut player_query: Query<(&Player, &mut Transform)>,
    wall_query: Query<&Transform, (With<TileCollider>, Without<Player>)>,
    keyboard: Res<Input<KeyCode>>,
    time: Res<Time>,
) {
    let (player, mut transform) = player_query.single_mut();

    let shift = player.speed * TILE_SIZE * time.delta_seconds();

    let mut x_delta = 0.0;
    if keyboard.pressed(KeyCode::D) {
        x_delta += shift;
    }
    if keyboard.pressed(KeyCode::A) {
        x_delta -= shift;
    }

    let mut y_delta = 0.0;
    if keyboard.pressed(KeyCode::W) {
        y_delta += shift;
    }
    if keyboard.pressed(KeyCode::S) {
        y_delta -= shift;
    }
    let target = transform.translation + Vec3::new(x_delta, 0.0, 0.0);
    if wall_collision_check(target, &wall_query) {
        transform.translation = target;
    }
    let target = transform.translation + Vec3::new(0.0, y_delta, 0.0);
    if wall_collision_check(target, &wall_query) {
        transform.translation = target;
    }
}

fn wall_collision_check(
    target_player_position: Vec3,
    wall_query: &Query<&Transform, (With<TileCollider>, Without<Player>)>,
) -> bool {
    for wall_transform in wall_query.iter() {
        let collision = collide(
            target_player_position,
            Vec2::splat(TILE_SIZE * 0.9),
            wall_transform.translation,
            Vec2::splat(TILE_SIZE),
        );
        if collision.is_some() {
            return false;
        }
    }
    true
}

fn spawn_player(mut commands: Commands, ascii: Res<AsciiSheet>) {
    let player_color = Color::rgb(0.3, 0.3, 0.9);
    let player_translation = Vec3::new(2.0 * TILE_SIZE, -2.0 * TILE_SIZE, 900.0);
    let player_name = "Player";

    let bg_color = Color::rgb(0.5, 0.5, 0.5);
    let bg_translation = Vec3::new(0.0, 0.0, -1.0);
    let bg_name = "Background";

    let player = spawn_ascii_sprite(
        &mut commands,
        &ascii,
        TileName::Player,
        player_color,
        player_translation,
    );
    let background = spawn_ascii_sprite(
        &mut commands,
        &ascii,
        TileName::Background,
        bg_color,
        bg_translation,
    );

    commands
        .entity(player)
        .insert(Name::new(player_name))
        .insert(Player { speed: 3.0 });

    commands.entity(background).insert(Name::new(bg_name));

    commands.entity(player).push_children(&[background]);
}
