#![allow(clippy::redundant_field_names)]

use crate::ascii::AsciiPlugin;
use crate::camera::CameraPlugin;
use crate::constants::RESOLUTION;
use crate::player::PlayerPlugin;
use crate::resource::ResourcePlugin;
use bevy::prelude::*;
use debug::DebugPlugin;
use tilemap::TileMapPlugin;

mod ascii;
mod camera;
mod constants;
mod debug;
mod player;
mod resource;
mod tilemap;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(ResourcePlugin)
        .add_plugin(CameraPlugin)
        .add_plugin(AsciiPlugin)
        .add_plugin(PlayerPlugin)
        .add_plugin(TileMapPlugin)
        .add_plugin(DebugPlugin)
        .run();
}
