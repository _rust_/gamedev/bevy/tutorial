use crate::RESOLUTION;
use bevy::prelude::*;

use crate::constants::CLEAR;

pub struct ResourcePlugin;

impl Plugin for ResourcePlugin {
    fn build(&self, app: &mut App) {
        let height = 100.0;
        app.insert_resource(ClearColor(CLEAR))
            .insert_resource(WindowDescriptor {
                width: height * RESOLUTION,
                height: height,
                title: "Bevy Tutorial".to_string(),
                resizable: false,
                ..Default::default()
            });
    }
}
